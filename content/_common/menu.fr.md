+++
+++

[Bienvenue](@/_index.md)

---

[Blog](@/blog/_index.fr.md)

---

[Forum](https://forum.joinjabber.org/)

---

[Chat](xmpp:chat@joinjabber.org?join)

---

[À propos](@/collective/_index.fr.md)
