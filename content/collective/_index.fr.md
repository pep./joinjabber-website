+++
title = "À propos du collectif JoinJabber"
+++

Dans cette section, tu trouveras les comptes-rendu de nos réunions. Si tu es intéresséE par nos objectifs, tu les trouveras [ici](@/collective/about/goals/index.fr.md).
